package com.example.routes

import com.example.models.Comentari
import com.example.models.Pelicula
import com.example.models.comentariStorage
import com.example.models.peliculaStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.filmRouting() {
    route("films") {
        get {
            if (peliculaStorage.isNotEmpty()) call.respond(peliculaStorage)
            else call.respondText("No contingut", status = HttpStatusCode.NoContent)
        }
        get("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (pelicula in peliculaStorage) {
                if (pelicula.id == id) return@get call.respond(pelicula)
            }
            call.respondText("No s'ha trobat la película amb $id", status = HttpStatusCode.NotFound)
        }
        post {
            val film = call.receive<Pelicula>()
            peliculaStorage.add(film)
            call.respondText("S'ha afegit correctament", status = HttpStatusCode.Created)
        }
        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (pelicula in peliculaStorage) {
                if (pelicula.id == id) {
                    peliculaStorage.remove(pelicula)
                    return@delete call.respondText(
                        "La pel·licula s'ha esborrat correctament",
                        status = HttpStatusCode.Accepted
                    )
                }
            }
        }
        put("{id?}"){
            if (call.parameters["id"].isNullOrBlank()) return@put call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            if (peliculaStorage.map { it.id }.contains(call.parameters["id"])) {
                peliculaStorage.removeAll { it.id == call.parameters["id"] }
                val filmupdate = call.receive<Pelicula>()
                peliculaStorage.add(filmupdate)
                call.respondText(
                    "S'ha actualitzat la pelicula amb id ${call.parameters["id"]}",
                    status = HttpStatusCode.Accepted
                )
            }
            val id = call.parameters["id"]
            for (pelicula in peliculaStorage) {
                if (pelicula.id == id) return@put call.respond(pelicula)
            }
            call.respondText("No s'ha trobat la película amb $id", status = HttpStatusCode.NotFound)
        }

    }

    route("{id?}/reviews") {
        post {
            val id = call.parameters["id"]
            if (id.isNullOrBlank()) return@post call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val newcoment = call.receive<Comentari>()
            comentariStorage.add(newcoment)
            call.respondText("S'ha afegit el comentari", status = HttpStatusCode.Created)
        }

        get {
            val id = call.parameters["id"]
            if (comentariStorage.isNotEmpty()){
                val listreviews = mutableListOf<Comentari>()
                for (comentari in comentariStorage){
                    if (comentari.id == id) listreviews.add(comentari)
                }
                call.respond(listreviews)
            }
            else call.respondText("Comentari no trobats", status = HttpStatusCode.NotFound)
        }

    }


}
