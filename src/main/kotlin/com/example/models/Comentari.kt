package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Comentari(
    val id: String,
    val idPelicula: Pelicula,
    val comentari: String,
    val datamade: String
)

val comentariStorage = mutableListOf<Comentari>()