package com.example.models

import kotlinx.serialization.Serializable
@Serializable
data class Pelicula (
    val id: String,
    val titol: String,
    val any: String,
    val genere: String,
    val director: String
)

val peliculaStorage = mutableListOf<Pelicula>()
